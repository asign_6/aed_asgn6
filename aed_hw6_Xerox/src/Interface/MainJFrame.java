/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.Business;
import Business.BusinessConfiguration.ConfigureBusiness;
import Business.Customer;
import Business.Market;
import Business.MarketOffer;




import Business.MarketRole;
import Business.Order;
import Business.OrderItem;


import Business.Product;
import Business.SalesPerson;
import Business.Supplier;
import Business.UserAccount;
import Business.UserRole;
import java.awt.CardLayout;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 *
 * @author Madhuja
 */
public class MainJFrame extends javax.swing.JFrame {

    /**
     * Creates new form MainJFrame
     */
    private Business b;
    public MainJFrame() {
        initComponents();
        b = ConfigureBusiness.Initialize();
        populatefromCsv(); 
    }
    public void populatefromCsv()
    {
        loadSuppliers();
        loadProducts();
       loadCustomers();
       loadSalesPersons();
       loadOrders();
       loadOrderItem();
       //loadOreder
    }
    public void loadSuppliers()
    {
        String csvFile="C:\\ProjectTest\\supplier.csv";
               BufferedReader br=null;
               SimpleDateFormat sf=new SimpleDateFormat("dd/MM/yyyy");
        String line="";
        Date dob=null;
        long sn=0;
         try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] supData = line.split(",");
                try{
                 sn=Long.parseLong(supData[5]);
                  dob=sf.parse(supData[6]);
                }
                catch(Exception e)
                {
                }
                  UserAccount ua=b.getUad().addUserAccount();
                  Supplier s=b.getSupplierDir().addSupplier();
                  ua.setUserId(supData[0]);
                  ua.setPassword(supData[1]);
                  ua.setRole(UserRole.Supplier);
                  ua.setAccountStatus(true);
                  ua.setPerson(s);
                  s.setFirstName(supData[2]);
                  s.setLastName(supData[3]);
                  s.setAddress(supData[4]);
              
                  s.setDob(dob);
                  s.setSsn(sn);
                
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    //e.printStackTrace();
                }
            }
        }
     
    }
    public void loadSalesPersons()
    {
       String csvFile="C:\\ProjectTest\\Salesperson.csv";
               BufferedReader br=null;
               SimpleDateFormat sf=new SimpleDateFormat("dd/MM/yyyy");
        String line="";
        Date dob=null;
        long sn=0;
         try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] supData = line.split(",");
                try{
                 sn=Long.parseLong(supData[5]);
                  dob=sf.parse(supData[6]);
                }
                catch(Exception e)
                {
                }
                  UserAccount ua=b.getUad().addUserAccount();
                 // Supplier s=b.getSupplierDir().addSupplier();
                 SalesPerson s=b.getSpdir().addSalesPerson();
                  ua.setUserId(supData[0]);
                  ua.setPassword(supData[1]);
                  ua.setRole(UserRole.SalesPerson);
                  ua.setAccountStatus(true);
                  ua.setPerson(s);
                  s.setFirstName(supData[2]);
                  s.setLastName(supData[3]);
                  s.setAddress(supData[4]);
              
                  s.setDob(dob);
                  s.setSsn(sn);
                
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    //e.printStackTrace();
                }
            }
        }
       
    }
    public void loadProducts()
    {
        String csvFile="C:\\ProjectTest\\Product.csv";
               BufferedReader br=null;
        String line="";
        int availQot=0;
        double floor=0.0;
        double ceil=0.0;
       // double target=0.0;
         try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] prodData = line.split(",");
                for(UserAccount ua:b.getUad().getLstUserAccounts())
                {                
                if(prodData[0].equalsIgnoreCase(ua.getUserId()))
                {
                    try
                    {
                        availQot=Integer.parseInt(prodData[3]);
                       
                       // target=Double.parseDouble(prodData[6]);

                        floor=Double.parseDouble(prodData[5]);
                        ceil=Double.parseDouble(prodData[4]);
                        //target=Double.parseDouble(prodData[6]);

                    }
                    catch(Exception etu)
                    {
                        
                    }
                    
                   Supplier s=(Supplier) ua.getPerson();
                   Product p=s.getProdCatalog().addProduct();
                         p.setProductName(prodData[1]);
                         p.setDescription(prodData[2]);
                         p.setQuantity(availQot);
                         p.setCeilPrice(ceil);
                         p.setFloorPrice(floor);
                        setMarketOffer(p);
                         //p.setTargetPrice(target);
                }
                
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    //e.printStackTrace();
                }
            }
        }
    }
    public void setMarketOffer(Product p)
    {
          double gap = (p.getCeilPrice()-p.getFloorPrice())/b.getMarketList().getLstMarket().size();
       
                           for(int i = 0; i < b.getMarketList().getLstMarket().size(); i++){
                        //if(m.getMarketRole().equals(b.getMarketList().getLstMarket().get(i).getMarketRole())){
                        MarketOffer mo  = b.getMarketOfferCatalog().addMarketOffer();
                            mo.setFloorprice(p.getFloorPrice()+i*gap);
                            mo.setCeilprice(mo.getFloorprice()+(i+1)*gap);
                            mo.setTargetprice((mo.getFloorprice()+mo.getCeilprice())/2);
                            mo.setProduct(p);
                            mo.setM(b.getMarketList().getLstMarket().get(i));
                        }

    }
    public void loadCustomers()
    {
        String csvFile="C:\\ProjectTest\\Market1.csv";
               BufferedReader br=null;
        String line="";
        //int availQot=0;
        //double floor=0.0;
        //double ceil=0.0;
        //double target=0.0;
         try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] custData = line.split(",");
               for(Market m:b.getMarketList().getLstMarket())
               {
                if(m.getMarketRole().name().equals(custData[1]))
                {
                    Customer c=m.addCustomer();
                    c.setAffiliation(custData[0]);
                    c.setRole(m.getMarketRole());
                    
                }
               }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    //e.printStackTrace();
                }
            }
        }
    }
    public void loadOrders()
    {

         String csvFile="C:\\ProjectTest\\OrderAssign.csv";

               BufferedReader br=null;
              // SimpleDateFormat sf=new SimpleDateFormat("dd/MM/yyyy");
        String line="";
      //  Date dob=null;
       //  long sn=0;
         try {
           
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] supData = line.split(",");
                 Order o =new Order();
                 o.setRole(MarketRole.valueOf(supData[0]));
                 SalesPerson sp7=null;

                 for(SalesPerson spgi:b.getSpdir().getLstsalesperson())
                 {
                     if(spgi.getFirstName().equals(supData[3]))
                     {
                         sp7=spgi;
                         break;
                     }
                 }
                 sp7.getLstOrderSales().add(o);

                 o.setSp(sp7);
                 Product pc =null;
                 double tar=0.0;
                 double flr=0.0;
                 double cei=0.0;
                 for(Supplier s:b.getSupplierDir().getLstSupplier())
                 {
                     for(Product p:s.getProdCatalog().getLstProducts())
                     {

                         if(p.getProductId()==(Integer.parseInt(supData[2])))
                         {
                             pc=p;
                         break;
                         }

                     }
                 }
                for(MarketOffer mo:b.getMarketOfferCatalog().getMarketOffer())
                {
                    if(mo.getProduct()==pc && mo.getM().getMarketRole().equals(o.getRole()))
                    {
                        tar=mo.getTargetprice();
                        flr=mo.getFloorprice();
                        cei=mo.getCeilprice();

                        break;

                    }
                }
                 Random r=new Random();
           // double ceilPrice=(double) tblProducts.getValueAt(selectedRow,4);
           // double floorPrice=(double) tblProducts.getValueAt(selectedRow,2);
            //ceilPrice=200;
           // floorPrice=100;
           // int b=r.nextInt(100-90)-10;
           double xk=0.0;
           if(flr>0 && cei>0)
           xk=r.nextInt((int) (cei-flr))+flr;
           
           //double price=computeActualPrice(cei,flr);
                   //pc.setProductId(Integer.parseInt(supData[2]));
                  //pc.setProductName(supData[3]);
               //OrderItem oi =  o.addOrderItem(pc,Integer.parseInt(supData[1]) , Double.parseDouble(supData[4]));
               OrderItem oi=o.addOrderItem(pc,Integer.parseInt(supData[1]),xk);
               oi.setTargetPrice(tar);
               
              
               
               b.getMasterOrderCatalog().addOrder(o);
            //     OrderItem oi=o.addOrderItem(o, WIDTH, ERROR);
                }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    //e.printStackTrace();
                }
            }
        }
     
        
  
    }
    public void loadOrderItem()
    {
         String csvFile="C:\\ProjectTest\\OrderItems1.csv";
               BufferedReader br=null;
              // SimpleDateFormat sf=new SimpleDateFormat("dd/MM/yyyy");
        String line="";
      //  Date dob=null;
       //  long sn=0;
         try {
           
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] supData = line.split(",");
               //  Order o =new Order();
               for(Order o:b.getMasterOrderCatalog().getLstOrders())
               {
                   
                   if(o.getOrderId()==Integer.parseInt(supData[4]))
                   {
                 o.setRole(MarketRole.valueOf(supData[0]));
                 SalesPerson sp7=null;
                 for(SalesPerson spgi:b.getSpdir().getLstsalesperson())
                 {
                     if(spgi.getFirstName().equals(supData[3]))
                     {
                         sp7=spgi;
                         break;
                     }
                 }
                 o.setSp(sp7);
                 Product pc =null;
                 double tar=0.0;
                 double flr=0.0;
                 double cei=0.0;
                 for(Supplier s:b.getSupplierDir().getLstSupplier())
                 {
                     for(Product p:s.getProdCatalog().getLstProducts())
                     {

                         if(p.getProductId()==(Integer.parseInt(supData[2])))
                         {
                             pc=p;
                         break;
                         }

                     }
                 }
                for(MarketOffer mo:b.getMarketOfferCatalog().getMarketOffer())
                {
                    if(mo.getProduct()==pc && mo.getM().getMarketRole().equals(o.getRole()))
                    {
                        tar=mo.getTargetprice();
                        flr=mo.getFloorprice();
                        cei=mo.getCeilprice();
                        break;
                    }
                }
                 Random r=new Random();
           // double ceilPrice=(double) tblProducts.getValueAt(selectedRow,4);
           // double floorPrice=(double) tblProducts.getValueAt(selectedRow,2);
            //ceilPrice=200;
           // floorPrice=100;
           // int b=r.nextInt(100-90)-10;
           double xk=r.nextInt((int) (cei-flr))+flr;
           //double price=computeActualPrice(cei,flr);
                   //pc.setProductId(Integer.parseInt(supData[2]));
                  //pc.setProductName(supData[3]);
               //OrderItem oi =  o.addOrderItem(pc,Integer.parseInt(supData[1]) , Double.parseDouble(supData[4]));
               OrderItem oi=o.addOrderItem(pc,Integer.parseInt(supData[1]),xk);
               oi.setTargetPrice(tar);
               break;
                   }
               }
               
               //b.getMasterOrderCatalog().addOrder(o);
            //     OrderItem oi=o.addOrderItem(o, WIDTH, ERROR);
                }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    //e.printStackTrace();
                }
            }
        }

    }
   
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel1 = new javax.swing.JPanel();
        LoginBtn = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        UserProcessContainer = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        LoginBtn.setText("LOGIN");
        LoginBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LoginBtnActionPerformed(evt);
            }
        });

        jButton1.setText("LOGOUT");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(LoginBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(160, 160, 160)
                .addComponent(LoginBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(57, 57, 57)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(266, Short.MAX_VALUE))
        );

        jSplitPane1.setLeftComponent(jPanel1);

        UserProcessContainer.setLayout(new java.awt.CardLayout());
        jSplitPane1.setRightComponent(UserProcessContainer);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 900, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void LoginBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LoginBtnActionPerformed

                UserLoginJpanel userloginjpanel  = new UserLoginJpanel(UserProcessContainer,b);
        UserProcessContainer.add("UserLoginJpanell",userloginjpanel);
        CardLayout layout  = (CardLayout)UserProcessContainer.getLayout();
        layout.next(UserProcessContainer);


        // TODO add your handling code here:
    }//GEN-LAST:event_LoginBtnActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
          UserProcessContainer.removeAll();
        UserLoginJpanel userloginjpanel  = new UserLoginJpanel(UserProcessContainer,b);
        UserProcessContainer.add("UserLoginJpanell",userloginjpanel);
        CardLayout layout  = (CardLayout)UserProcessContainer.getLayout();
        layout.next(UserProcessContainer);

    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainJFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton LoginBtn;
    private javax.swing.JPanel UserProcessContainer;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSplitPane jSplitPane1;
    // End of variables declaration//GEN-END:variables
}
