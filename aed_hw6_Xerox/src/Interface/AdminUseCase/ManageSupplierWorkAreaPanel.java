/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.AdminUseCase;

import Business.Business;
import Business.Product;
import Business.Supplier;
import Business.UserAccount;
import Business.UserRole;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CHINMAY
 */
public class ManageSupplierWorkAreaPanel extends javax.swing.JPanel {

    /**
     * Creates new form ManageSupplierWorkAreaPanel
     */
   JPanel UserProcessContainer;
   Business b;
   UserAccount ua;
   public ManageSupplierWorkAreaPanel(JPanel UserProcessContainer, Business b,UserAccount ua) {
        initComponents();
        this.UserProcessContainer = UserProcessContainer;
        this.b  = b;
        this.ua =ua;
        populatetab();
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    public void populatetab()
    {
      DefaultTableModel dtm  = (DefaultTableModel)SupplierTable.getModel();
        dtm.setRowCount(0);
        for(UserAccount a : b.getUad().getLstUserAccounts())
            if(a.getRole().equals(UserRole.Supplier))
       // for(Supplier s : b.getSupplierDir().getLstSupplier())
        {
                int c =  0;
           Object []row  = new Object[3];
           row[1] = a.getPerson();
           row[0] = a;
           Supplier s  = (Supplier)a.getPerson();
           for(Product p : s.getProdCatalog().getLstProducts())
           {
               c++;
           }
           row[2]= c;
          //  row[1] = account.getPwd();
         //  row[2] = account.getRole() ;
         //  row[3] = account.getBalance();
           
           dtm.addRow(row);
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        SupplierTable = new javax.swing.JTable();
        newSupplierBtn = new javax.swing.JButton();
        updateSupplierBtn = new javax.swing.JButton();
        deleteSupplierBtn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        btnBack = new javax.swing.JButton();

        SupplierTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "User Id", "Supplier Name", "No Of Products"
            }
        ));
        jScrollPane1.setViewportView(SupplierTable);

        newSupplierBtn.setText("New Supplier");
        newSupplierBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newSupplierBtnActionPerformed(evt);
            }
        });

        updateSupplierBtn.setText("Update Supplier");
        updateSupplierBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateSupplierBtnActionPerformed(evt);
            }
        });

        deleteSupplierBtn.setText("Delete");
        deleteSupplierBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteSupplierBtnActionPerformed(evt);
            }
        });

        jLabel1.setText("MANAGE Supplier");

        btnBack.setText("<<Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(81, 81, 81)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(newSupplierBtn)
                                .addGap(94, 94, 94)
                                .addComponent(updateSupplierBtn)
                                .addGap(68, 68, 68)
                                .addComponent(deleteSupplierBtn))
                            .addComponent(btnBack)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(11, 11, 11)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(276, 276, 276)
                        .addComponent(jLabel1)))
                .addContainerGap(238, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 85, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(newSupplierBtn)
                    .addComponent(updateSupplierBtn)
                    .addComponent(deleteSupplierBtn))
                .addGap(41, 41, 41)
                .addComponent(btnBack)
                .addGap(37, 37, 37))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void newSupplierBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newSupplierBtnActionPerformed
     CreateSupplierPanel createsupplierpanel = new CreateSupplierPanel(UserProcessContainer,b);
        UserProcessContainer.add("CreateSupplierPanel",createsupplierpanel);
        CardLayout layout = (CardLayout) UserProcessContainer.getLayout();
        layout.next(UserProcessContainer);      

        // TODO add your handling code here:
    }//GEN-LAST:event_newSupplierBtnActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        UserProcessContainer.remove(this);
        CardLayout layout=(CardLayout) UserProcessContainer.getLayout();
        layout.previous(UserProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void deleteSupplierBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteSupplierBtnActionPerformed
        // TODO add your handling code here:
        int selectedRow=SupplierTable.getSelectedRow();
        try
        {
        if(selectedRow>-1)
        {
            UserAccount ua=(UserAccount) SupplierTable.getValueAt(selectedRow,0);
            int op=JOptionPane.showConfirmDialog(null,"Do you want to delete this supplier");
            if(op==0)
            {
                Supplier sd=(Supplier) ua.getPerson();
                b.getSupplierDir().removeSupplier(sd);
               b.getUad().removeUserAccount(ua);
               JOptionPane.showMessageDialog(null,"Supplier account deleted successfully");
               populatetab();
            }
            
        }
        else
        {
            JOptionPane.showMessageDialog(null,"Please select a row from table","Warning",JOptionPane.WARNING_MESSAGE);
        }
        }
        catch(Exception ei)
        {
         JOptionPane.showMessageDialog(null,"Sorry! something went wrong","Warning",JOptionPane.WARNING_MESSAGE);
        }

    }//GEN-LAST:event_deleteSupplierBtnActionPerformed

    private void updateSupplierBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateSupplierBtnActionPerformed
        // TODO add your handling code here:
         int selectedRow=SupplierTable.getSelectedRow();       
        if(selectedRow>-1)
        {
            UserAccount ua=(UserAccount) SupplierTable.getValueAt(selectedRow,0);
             UpdateSupplierJPanel usjp=new UpdateSupplierJPanel(UserProcessContainer,b,ua);
              UserProcessContainer.add("UpdateSupplierJPanel",usjp);
        CardLayout layout  = (CardLayout)UserProcessContainer.getLayout();
        layout.next(UserProcessContainer);
            
        }
        else
        {
            JOptionPane.showMessageDialog(null,"Please select a row from table","Warning",JOptionPane.WARNING_MESSAGE);
        }
        
       
        
    }//GEN-LAST:event_updateSupplierBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable SupplierTable;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton deleteSupplierBtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton newSupplierBtn;
    private javax.swing.JButton updateSupplierBtn;
    // End of variables declaration//GEN-END:variables
}
