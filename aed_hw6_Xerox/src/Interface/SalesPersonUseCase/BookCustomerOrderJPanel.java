/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.SalesPersonUseCase;

import Business.Business;
import Business.Customer;
import Business.MarketOffer;
import Business.MarketRole;
import Business.Order;
import Business.OrderItem;
import Business.Product;
import Business.SalesPerson;
import Business.Supplier;
import java.awt.CardLayout;
import java.util.Random;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Chaitanya
 */
public class BookCustomerOrderJPanel extends javax.swing.JPanel {

    /**
     * Creates new form BookCustomerOrderJPanel
     */
    private JPanel UserProcessContainer;
    private Business b;
    private Customer c ;
    private Order order;
    private boolean isCheckedOut=false;
    private SalesPerson sp;
    public BookCustomerOrderJPanel(JPanel UserProcessContainer, Business b, Customer c,SalesPerson sp) {
       initComponents();
       this.UserProcessContainer=UserProcessContainer;
       this.b=b;
       this.c=c;
       this.sp=sp;
       populateSupplier();
       if(!isCheckedOut)
       order =new Order();
    }
public void populateSupplier()
{
    for(Supplier s:b.getSupplierDir().getLstSupplier())
    {
        cbSuppliers.addItem(s);
        
    }
    populateProducts();
}
public void populateProducts()
{
    Supplier sup=(Supplier) cbSuppliers.getSelectedItem();
    DefaultTableModel dtm=(DefaultTableModel) tblProducts.getModel();
    dtm.setRowCount(0);
    double target=0.0d;
    double floor=0.0d;
    double ceil=0.0d;
    for(Product p: sup.getProdCatalog().getLstProducts())
    {
//        for( Map.Entry<MarketRole,MarketOffer> mapet:p.getHmOffer().entrySet())
//        {
//            if(mapet.getKey().equals(c.getRole()))
//            {
//                target = mapet.getValue().getTargetprice();
//                floor=mapet.getValue().getFloorprice();
//                ceil=mapet.getValue().getCeilprice();
//            }
//        }
        for(MarketOffer mo:b.getMarketOfferCatalog().getMarketOffer())
        {
            if(mo.getM().getMarketRole().equals(c.getRole()) && mo.getProduct()==p)
           {
             target=  mo.getTargetprice();
             floor=mo.getFloorprice();
             ceil=mo.getCeilprice();
           }
        }
       // 
       // if(ceil>0)
//        if(ceil<=0 || floor <=0 || target <=0)
//        {
//          JOptionPane.showMessageDialog(null,"There is no market offer set for this product");
//          return;
//        }
        Object[] row=new Object[6];
        row[0]=p;
        row[1]=p.getProductName();
        row[2]=floor;
        row[3]=target;
        row[4]=ceil;
        row[5]=p.getQuantity();
        dtm.addRow(row);
        
    }
}
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        cbSuppliers = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblProducts = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblOrders = new javax.swing.JTable();
        btnRemove = new javax.swing.JButton();
        btnCheckout = new javax.swing.JButton();
        btnAddToCart = new javax.swing.JButton();
        spinnerQuantity = new javax.swing.JSpinner();
        lblQuantity = new javax.swing.JLabel();
        btnModify = new javax.swing.JButton();
        txtModifiedQt = new javax.swing.JTextField();
        btnSearch = new javax.swing.JButton();
        txtSearchProduct = new javax.swing.JTextField();
        btnViewOrderItem = new javax.swing.JButton();
        btnNegotiate = new javax.swing.JButton();

        jLabel1.setText("Select Supplier:");

        cbSuppliers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbSuppliersActionPerformed(evt);
            }
        });

        tblProducts.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Product Id", "Product Name", "Floor Price", "Target Price", "Ceiling Price", "Quantity"
            }
        ));
        jScrollPane1.setViewportView(tblProducts);

        tblOrders.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Product Id", "Product Name", "Actual Price", "Order Quantity"
            }
        ));
        jScrollPane2.setViewportView(tblOrders);

        btnRemove.setText("Remove OrderItem");
        btnRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveActionPerformed(evt);
            }
        });

        btnCheckout.setText("checkOut");
        btnCheckout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckoutActionPerformed(evt);
            }
        });

        btnAddToCart.setText("Add to cart");
        btnAddToCart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddToCartActionPerformed(evt);
            }
        });

        lblQuantity.setText("Quantity:");

        btnModify.setText("Modify Quantity");
        btnModify.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModifyActionPerformed(evt);
            }
        });

        btnSearch.setText("Search");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        btnViewOrderItem.setText("View Order Item");
        btnViewOrderItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewOrderItemActionPerformed(evt);
            }
        });

        btnNegotiate.setText("Negotiate");
        btnNegotiate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNegotiateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(92, 92, 92)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnViewOrderItem, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(52, 52, 52)
                        .addComponent(btnModify)
                        .addGap(29, 29, 29)
                        .addComponent(txtModifiedQt, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(90, 90, 90)
                        .addComponent(lblQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(spinnerQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(269, 269, 269)
                        .addComponent(btnAddToCart, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 800, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(54, 54, 54)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnRemove, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnCheckout, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnNegotiate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(507, 507, 507))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(46, 46, 46)
                        .addComponent(cbSuppliers, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtSearchProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(369, 369, 369))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(txtSearchProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cbSuppliers, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(44, 44, 44)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 60, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnAddToCart)
                            .addComponent(spinnerQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblQuantity))
                        .addGap(56, 56, 56)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnModify)
                            .addComponent(txtModifiedQt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnViewOrderItem))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnNegotiate)
                        .addGap(27, 27, 27)
                        .addComponent(btnRemove)
                        .addGap(46, 46, 46)
                        .addComponent(btnCheckout)
                        .addGap(93, 93, 93))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void cbSuppliersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbSuppliersActionPerformed
        // TODO add your handling code here:
        populateProducts();
    }//GEN-LAST:event_cbSuppliersActionPerformed

    private void btnAddToCartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddToCartActionPerformed
        // TODO add your handling code here:
        int selectedRow=tblProducts.getSelectedRow();
        try{
        if(selectedRow>-1)
        {
            
            Product p=(Product) tblProducts.getValueAt(selectedRow,0);
            int qot=(int) spinnerQuantity.getValue();
            if(qot<=0)
            {
                JOptionPane.showMessageDialog(null,"Quantity cannot be less than or equal to zero","Warning",JOptionPane.WARNING_MESSAGE);
                return;
            }
            Random r=new Random();
            double ceilPrice=(double) tblProducts.getValueAt(selectedRow,4);
            double floorPrice=(double) tblProducts.getValueAt(selectedRow,2);

           // int b=r.nextInt(100-90)-10;
          // double price=computeActualPrice(ceilPrice,floorPrice);
            double price=r.nextInt((int) (ceilPrice-floorPrice))+floorPrice;
            //Rand  rnos=new 
            if(qot<=p.getQuantity())
            {
                boolean alreadyPresent=false;
                for(OrderItem oi:order.getLstOrders())
                {
                    if(oi.getProd()==p)
                    {
                        int oldAvail=p.getQuantity();
                        int newAvail=oldAvail-qot;
                        p.setQuantity(newAvail);
                        oi.setOrderQuantity(qot+oi.getOrderQuantity());
                        alreadyPresent=true;
                        populateProducts();
                        refreshOrderTable();
                        break;
                        
                    }
                }
                if(!alreadyPresent)
                {
                    int oldAvail=p.getQuantity();
                    int newAvail=oldAvail-qot;
                    p.setQuantity(newAvail);
                    order.addOrderItem(p, qot, price);
                    order.setRole(c.getRole());
                    populateProducts();
                        refreshOrderTable();
                }
            }
            else
            {
                JOptionPane.showMessageDialog(null,"quantity is greater than product availability","Warning",JOptionPane.WARNING_MESSAGE);
            }
        }
        else
        {
          JOptionPane.showMessageDialog(null,"Please select a product","Warning",JOptionPane.WARNING_MESSAGE);  
        }
        }
        catch(Exception ea)
        {
            
        }
    }//GEN-LAST:event_btnAddToCartActionPerformed
public double computeActualPrice(double cp,double fp)
{
  Random r=new Random();
  
 double xk=r.nextInt((int) (cp-fp))+fp;
 //test code
 JOptionPane msg = new JOptionPane("Negotiating with customer....", JOptionPane.WARNING_MESSAGE);
    final JDialog dlg = msg.createDialog("Information");
    dlg.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
    new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          Thread.sleep(2500);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        dlg.setVisible(false);
      }
    }).start();
    dlg.setVisible(true);
 //end test code
  return xk;
}
 
    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        // TODO add your handling code here:
        if(txtSearchProduct.getText().isEmpty())
        {
            JOptionPane.showMessageDialog(null,"Please enter product name","Warning",JOptionPane.WARNING_MESSAGE);
        }
        else
        {
            refreshProductsTable(txtSearchProduct.getText());
        }
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnViewOrderItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewOrderItemActionPerformed
        // TODO add your handling code here:
         int selectedRow=tblOrders.getSelectedRow();
         if(selectedRow>-1)
         {
             OrderItem oi=(OrderItem) tblOrders.getValueAt(selectedRow,0);
             ViewOrderItemJPanel voijp=new ViewOrderItemJPanel(UserProcessContainer,oi);
             UserProcessContainer.add("ViewOrderItemJPanel",voijp);
             CardLayout layout  = (CardLayout)UserProcessContainer.getLayout();
            layout.next(UserProcessContainer);        
         }
         else
         {
            JOptionPane.showMessageDialog(null,"Please select a order item","Warning",JOptionPane.WARNING_MESSAGE);   
         }
         
    }//GEN-LAST:event_btnViewOrderItemActionPerformed

    private void btnModifyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModifyActionPerformed
        // TODO add your handling code here:        
     int selectedRow=tblOrders.getSelectedRow();
     try{
     if(txtModifiedQt.getText().isEmpty())
     {
         JOptionPane.showMessageDialog(null,"Please select a valid quantity","Warning",JOptionPane.WARNING_MESSAGE);   
         return;
     }
     
         if(selectedRow>-1)
         {
             OrderItem oi=(OrderItem) tblOrders.getValueAt(selectedRow,0);
             int currAvail=oi.getProd().getQuantity();
             int oldQty=oi.getOrderQuantity();
             int newQty=Integer.parseInt(txtModifiedQt.getText());
             if(newQty>(currAvail+oldQty))
             {
               JOptionPane.showMessageDialog(null,"Quantity cannot be greater than available quantity","Warning",JOptionPane.WARNING_MESSAGE);     
               return;
             }
             if(newQty<=0)
             {
                JOptionPane.showMessageDialog(null,"Quantity cannot be less than zero","Warning",JOptionPane.WARNING_MESSAGE);     
               return; 
             }
             oi.setOrderQuantity(newQty);
             oi.getProd().setQuantity(currAvail+(oldQty-newQty));
             refreshOrderTable();
             populateProducts();
         }
         else
         {
            JOptionPane.showMessageDialog(null,"Please select a order item","Warning",JOptionPane.WARNING_MESSAGE);   
         }
     }
     catch(Exception ei)
     {
        JOptionPane.showMessageDialog(null,"Please enter valid values","Warning",JOptionPane.WARNING_MESSAGE);
        return ;
     }
    }//GEN-LAST:event_btnModifyActionPerformed

    private void btnCheckoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckoutActionPerformed
        // TODO add your handling code here:
        if(order.getLstOrders().size()>0)
        {
            ////TODO# Add order item to masterOrderItem
            order.setSp(sp);
            sp.getLstOrderSales().add(order);
            b.getMasterOrderCatalog().addOrder(order);
            isCheckedOut=true;
            JOptionPane.showMessageDialog(null,"Order booked successfully");
            ((DefaultTableModel) tblOrders.getModel()).setRowCount(0);
            
        }
        else
        {
         JOptionPane.showMessageDialog(null,"Your cart is empty","Warning",JOptionPane.WARNING_MESSAGE);   
        }
    }//GEN-LAST:event_btnCheckoutActionPerformed

    private void btnRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveActionPerformed
        // TODO add your handling code here:
        int selectedRow=tblOrders.getSelectedRow();
         if(selectedRow>-1)
         {
             int op=JOptionPane.showConfirmDialog(null,"Are you sure you want to remove item");
             if(op==0)
             {
             OrderItem oi=(OrderItem) tblOrders.getValueAt(selectedRow,0);
                int oldAvail=oi.getProd().getQuantity();
                int oldQty=oi.getOrderQuantity();
                int newQty=oldAvail+oldQty;
                oi.getProd().setQuantity(newQty);
                order.removeOrderItem(oi);
                refreshOrderTable();
             }
         }
         else
         {
            JOptionPane.showMessageDialog(null,"Please select a order item","Warning",JOptionPane.WARNING_MESSAGE);   
         }
    }//GEN-LAST:event_btnRemoveActionPerformed

    private void btnNegotiateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNegotiateActionPerformed
        // TODO add your handling code here:
       int selectedRow=tblOrders.getSelectedRow();
         if(selectedRow>-1)
         {
             
             OrderItem oi=(OrderItem) tblOrders.getValueAt(selectedRow,0);
//                int oldAvail=oi.getProd().getQuantity();
//                int oldQty=oi.getOrderQuantity();
//                int newQty=oldAvail+oldQty;
//                oi.getProd().setQuantity(newQty);
//                order.removeOrderItem(oi);
//double tar=0.0;
double flr=0.0;
double cle=0.0;
for(MarketOffer mo:b.getMarketOfferCatalog().getMarketOffer())
        {
            if(mo.getM().getMarketRole().equals(c.getRole()) && mo.getProduct()==oi.getProd())
           {
             //tar=  mo.getTargetprice();
             flr=mo.getFloorprice();
             cle=mo.getCeilprice();
           }
        }
                double price=computeActualPrice(cle,flr);
                order.updateOrderItem(oi, price);

                refreshOrderTable();
             
         }
         else
         {
            JOptionPane.showMessageDialog(null,"Please select a order item","Warning",JOptionPane.WARNING_MESSAGE);   
         }
    }//GEN-LAST:event_btnNegotiateActionPerformed
public void refreshProductsTable(String prodname)
{
    DefaultTableModel dtm=(DefaultTableModel) tblProducts.getModel();
    dtm.setRowCount(0);
    double target=0.0d;
    double floor=0.0d;
    double ceil=0.0d;
    for(Supplier s1: b.getSupplierDir().getLstSupplier())
    {
    for(Product p: s1.getProdCatalog().getLstProducts())
    {
        if(p.getProductName().equalsIgnoreCase(prodname))
        {
//        for( Map.Entry<MarketRole,MarketOffer> mapet:p.getHmOffer().entrySet())
//        {
//            if(mapet.getKey().equals(c.getRole()))
//            {
//                target = mapet.getValue().getTargetprice();
//                floor=mapet.getValue().getFloorprice();
//                ceil=mapet.getValue().getCeilprice();
//            }
//        }
        for(MarketOffer mo:b.getMarketOfferCatalog().getMarketOffer())
        {
           if(mo.getM().getMarketRole().equals(c.getRole()) && mo.getProduct()==p)
           {
             target=  mo.getTargetprice();
             floor=mo.getFloorprice();
             ceil=mo.getCeilprice();
           }
        }
        Object[] row=new Object[6];
        row[0]=p.getProductId();
        row[1]=p.getProductName();
        row[2]=floor;
        row[3]=target;
        row[4]=ceil;
        row[5]=p.getQuantity();
        dtm.addRow(row);
        }
    }
    }
}
public void refreshOrderTable()
{
    DefaultTableModel dtm=(DefaultTableModel) tblOrders.getModel();
    dtm.setRowCount(0);
   // double target=0.0d;
    //double floor=0.0d;
    //double ceil=0.0d;
    
    for(OrderItem oi: order.getLstOrders())
    {
        
        Object[] row=new Object[4];
        row[0]=oi;
        row[1]=oi.getProd().getProductName();
        row[3]=oi.getOrderQuantity();
        row[2]=oi.computeSalesPerQt(oi.getSalesPrice(), oi.getOrderQuantity());
        
        dtm.addRow(row);
        
    }

}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddToCart;
    private javax.swing.JButton btnCheckout;
    private javax.swing.JButton btnModify;
    private javax.swing.JButton btnNegotiate;
    private javax.swing.JButton btnRemove;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnViewOrderItem;
    private javax.swing.JComboBox cbSuppliers;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblQuantity;
    private javax.swing.JSpinner spinnerQuantity;
    private javax.swing.JTable tblOrders;
    private javax.swing.JTable tblProducts;
    private javax.swing.JTextField txtModifiedQt;
    private javax.swing.JTextField txtSearchProduct;
    // End of variables declaration//GEN-END:variables
}
