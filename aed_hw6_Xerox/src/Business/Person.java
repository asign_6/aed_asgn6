/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Chaitanya
 */
public class Person {
    private String firstName;
    private String lastName;
    private long ssn;
    private Date dob;
    private String address;
    private ArrayList<UserAccount> lstUserAccounts;

    public ArrayList<UserAccount> getLstUserAccounts() {
        return lstUserAccounts;
    }

    public void setLstUserAccounts(ArrayList<UserAccount> lstUserAccounts) {
        this.lstUserAccounts = lstUserAccounts;
    }

    public Person() {
        lstUserAccounts=new ArrayList<>();
    }
    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public long getSsn() {
        return ssn;
    }

    public void setSsn(long ssn) {
        this.ssn = ssn;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    @Override
    public String toString()
    {
        return getFirstName()+" "+getLastName();
    }
    public void removeUserFromPerson(UserAccount user)
    {
     lstUserAccounts.remove(user);
    } 
   
}
