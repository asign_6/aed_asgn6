/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author CHINMAY
 */
public class SalesPerson extends Person{
    
    private double commission ;
    private ArrayList<Order> lstOrderSales;

    public ArrayList<Order> getLstOrderSales() {
        return lstOrderSales;
    }

    public void setLstOrderSales(ArrayList<Order> lstOrderSales) {
        this.lstOrderSales = lstOrderSales;
    }
    public SalesPerson()
    {
      lstOrderSales=new ArrayList<Order>();
    
    }

    public double getCommission() {
        return commission;
    }

    public void setCommission(double commission) {
        this.commission = commission;
    }
     @Override
    public String toString()
    {
      return this.getFirstName() +" "+this.getLastName();
    }
    
}
