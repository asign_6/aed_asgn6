/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Chaitanya
 */
public class Business {
   private SupplierDirectory supplierDir;
   private MarketOfferCatalog marketOfferCatalog;
   private MarketList marketList;
   private MasterOrderCatalog masterOrderCatalog;

    public MasterOrderCatalog getMasterOrderCatalog() {
        return masterOrderCatalog;
    }

    public void setMasterOrderCatalog(MasterOrderCatalog masterOrderCatalog) {
        this.masterOrderCatalog = masterOrderCatalog;
    }
   private PersonDirectory personDirectory;
   private SalespersonDirectory spdir;

    public SalespersonDirectory getSpdir() {
        return spdir;
    }

    public void setSpdir(SalespersonDirectory spdir) {
        this.spdir = spdir;
    }
   private UserAccountDirectory uad;

    public PersonDirectory getPersonDirectory() {
        return personDirectory;
    }

    public void setPersonDirectory(PersonDirectory personDirectory) {
        this.personDirectory = personDirectory;
    }

    public Business() {
        supplierDir=new SupplierDirectory();
        marketList=new MarketList();
        marketOfferCatalog=new MarketOfferCatalog();
        personDirectory=new PersonDirectory();
        uad=new UserAccountDirectory();
        spdir =new SalespersonDirectory();
        masterOrderCatalog=new MasterOrderCatalog();
    }

    public UserAccountDirectory getUad() {
        return uad;
    }

    public void setUad(UserAccountDirectory uad) {
        this.uad = uad;
    }

    public SupplierDirectory getSupplierDir() {
        return supplierDir;
    }

    public void setSupplierDir(SupplierDirectory supplierDir) {
        this.supplierDir = supplierDir;
    }

    public MarketOfferCatalog getMarketOfferCatalog() {
        return marketOfferCatalog;
    }

    public void setMarketOfferCatalog(MarketOfferCatalog marketOfferCatalog) {
        this.marketOfferCatalog = marketOfferCatalog;
    }

    public MarketList getMarketList() {
        return marketList;
    }

    public void setMarketList(MarketList marketList) {
        this.marketList = marketList;
    }
   
}
