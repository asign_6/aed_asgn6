/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Chaitanya
 */
public class PersonDirectory {
    private ArrayList<Person>persondir;
    
    public PersonDirectory()
    {
    
    persondir = new ArrayList<Person>();
    }     

    public ArrayList<Person> getPersondir() {
        return persondir;
    }

    public void setPersondir(ArrayList<Person> persondir) {
        this.persondir = persondir;
    }
    public Person addPerson()
    {
        Person p  = new Person();
        persondir.add(p);
        return p;
    
    
    }
    public void addPerson(SalesPerson sep)
    {
        persondir.add(sep);
    }
    public void addPerson(Supplier spu)
    {
        persondir.add(spu);
    }
}
