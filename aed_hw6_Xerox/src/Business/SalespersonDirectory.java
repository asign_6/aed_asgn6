/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author CHINMAY
 */
public class SalespersonDirectory {
    
    private ArrayList <SalesPerson> lstsalesperson;

    public SalespersonDirectory() {
        lstsalesperson = new ArrayList<SalesPerson>();
    }

    public ArrayList<SalesPerson> getLstsalesperson() {
        return lstsalesperson;
    }

    public void setLstsalesperson(ArrayList<SalesPerson> lstsalesperson) {
        this.lstsalesperson = lstsalesperson;
    }
    
   public void addSalesPerson(SalesPerson sp)
   {
    lstsalesperson.add(sp);
   }
   public SalesPerson addSalesPerson()
   {
       SalesPerson sp=new SalesPerson();
       lstsalesperson.add(sp);
       return sp;
   }
   public SalesPerson addSalesPerson(Person p)
   {
      // p=new SalesPerson();
       SalesPerson sp=new SalesPerson();
      
       //sp=(SalesPerson) p;
       
       lstsalesperson.add(sp);
       return sp;
   }
    public void removeSalesPerson(SalesPerson sp)
    {
      lstsalesperson.remove(sp);
    }
}
