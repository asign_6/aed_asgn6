/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Chaitanya
 */
public class OrderItem {
 private Product prod;
 private int orderQuantity;
 private double salesPrice;
 private double targetPrice;

    public double getTargetPrice() {
        return targetPrice;
    }

    public void setTargetPrice(double targetPrice) {
        this.targetPrice = targetPrice;
    }

    public double getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(double salesPrice) {
        this.salesPrice = salesPrice;
    }

    public int getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(int orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public Product getProd() {
        return prod;
    }

    public void setProd(Product prod) {
        this.prod = prod;
    }

    public OrderItem() {
        prod=new Product();
    }
@Override
public String toString()
{
    return String.valueOf(this.getProd().getProductId());
}
   public double computeSalesPerQt(double sp,int qt)
   {
       return sp*qt;
   }
}
