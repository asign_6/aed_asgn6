/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Chaitanya
 */
public class MarketOffer {
    //private int quantity;
    private double floorprice;
    private double targetprice;
    private double ceilprice;
    //private MarketRole role; 
    private Market m;

public Market getM()
{
        return m;
}

//    public MarketRole getRole() {
//        return role;
//    }
//
//    public void setRole(MarketRole role) {
//        this.role = role;
//    }
//    public MarketOffer(Product p, Market m) {
//        product = p;
//        this.m = m;
//    }
    public void setM(Market m) {
        this.m = m;
    }

    public MarketOffer() {
        m=new Market();
        product=new Product();
    }
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
    private Product product;     //the product will remain same but the price will vary acc to the market

//    public int getQuantity() {
//        return quantity;
//    }
//
//    public void setQuantity(int quantity) {
//        this.quantity = quantity;
//    }

    public double getFloorprice() {
        return floorprice;
    }

    public void setFloorprice(double floorprice) {
        this.floorprice = floorprice;
    }

    public double getTargetprice() {
        return targetprice;
    }

    public void setTargetprice(double targetprice) {
        this.targetprice = targetprice;
    }

    public double getCeilprice() {
        return ceilprice;
    }

    public void setCeilprice(double ceilprice) {
        this.ceilprice = ceilprice;
    }

//    public Product getProduct() {
//        return product;
//    }
//
//    public void setProduct(Product product) {
//        this.product = product;
//    }

   
    
    
}
