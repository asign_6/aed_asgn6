/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Madhuja
 */
public class Supplier extends Person {
    private ProductCatalog prodCatalog;
   // private UserAccount suppAccount;

    public Supplier() {
        prodCatalog=new ProductCatalog();
       // suppAccount=new UserAccount();
    }

//    public UserAccount getSuppAccount() {
//        return suppAccount;
//    }
//
//    public void setSuppAccount(UserAccount suppAccount) {
//        this.suppAccount = suppAccount;
//    }
    

    public ProductCatalog getProdCatalog() {
        return prodCatalog;
    }

    public void setProdCatalog(ProductCatalog prodCatalog) {
        this.prodCatalog = prodCatalog;
    }
    @Override
    public String toString()
    {
      return this.getFirstName() +" "+this.getLastName();
    }
}