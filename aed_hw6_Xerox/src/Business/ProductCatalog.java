/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Madhuja
 */
public class ProductCatalog {
    private ArrayList<Product> lstProducts;
   
    public ProductCatalog() {
        lstProducts=new ArrayList<Product>();
    }

    public ArrayList<Product> getLstProducts() {
        return lstProducts;
    }

    public void setLstProducts(ArrayList<Product> lstProducts) {
        this.lstProducts = lstProducts;
    }
    public Product addProduct()
    {
        Product p=new Product();
        lstProducts.add(p);
        return p;
    }
    public void RemoveProduct(Product p)
    {
        lstProducts.remove(p);
    }
    
    public Product searchProduct(int productID){
        for(Product p : lstProducts){
            if(p.getProductId()==productID){
                return p;
            }
        }
        return null;
    }
}
