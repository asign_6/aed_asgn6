/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Chaitanya
 */
public class MasterOrderCatalog {
   private ArrayList<Order> lstOrders;

    public ArrayList<Order> getLstOrders() {
        return lstOrders;
    }

    public void setLstOrders(ArrayList<Order> lstOrders) {
        this.lstOrders = lstOrders;
    }
   public MasterOrderCatalog()
   {
       lstOrders=new ArrayList<Order>();
   }
   
public void addOrder(Order o)
{
    lstOrders.add(o);
}
    
}
