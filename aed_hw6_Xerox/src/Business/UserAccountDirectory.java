/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Chaitanya
 */
public class UserAccountDirectory {
  private ArrayList<UserAccount> lstUserAccounts;

    public UserAccountDirectory() {
        lstUserAccounts=new ArrayList<UserAccount>();
    }

    public ArrayList<UserAccount> getLstUserAccounts() {
        return lstUserAccounts;
    }

    public void setLstUserAccounts(ArrayList<UserAccount> lstUserAccounts) {
        this.lstUserAccounts = lstUserAccounts;
    }
  public UserAccount addUserAccount()
  {
     UserAccount u  = new UserAccount();
     lstUserAccounts.add(u);
     return u;
  
  }
  
  public UserAccount isValid(String un, String pwd)
  {
      for(UserAccount ac : lstUserAccounts)
      {
          if(ac.getUserId().equals(un)&&ac.getPassword().equals(pwd)&& ac.getAccountStatus())
          {
              return ac;
          }
              
      
      }return null;
 
  }
    public boolean isUnique(String un)
    {
       for(UserAccount ac  : lstUserAccounts)
       {
           if(ac.getUserId().equals(un))
           {
             return false;
           }
       
       
       }
       return true;
    
    } 
    public void removeUserAccount(UserAccount ua)
    {
        lstUserAccounts.remove(ua);
    }

}

