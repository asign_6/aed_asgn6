/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Chaitanya
 */
public class Order {



private ArrayList<OrderItem> lstOrders;   
private SalesPerson sp;
private MarketRole role;
private int orderId;
private static int count;


    public MarketRole getRole() {
        return role;
    }

    public void setRole(MarketRole role) {
        this.role = role;
    }


    public SalesPerson getSp() {
        return sp;
    }

    public void setSp(SalesPerson sp) {
        this.sp = sp;
    }
    public Order() {
        lstOrders=new ArrayList<OrderItem>();
        sp=new SalesPerson();
        count++;
        orderId = count;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public ArrayList<OrderItem> getLstOrders() {
        return lstOrders;
    }

    public void setLstOrders(ArrayList<OrderItem> lstOrders) {
        this.lstOrders = lstOrders;
    }
    private int orderNumber;

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }
public OrderItem addOrderItem(Product p,int q, double price)
{
    OrderItem oi=new OrderItem();
    oi.setProd(p);
    oi.setOrderQuantity(q);
    oi.setSalesPrice(price);
    lstOrders.add(oi);
    return oi;
}
public void updateOrderItem(OrderItem oit,double price)
{
    for(OrderItem oi:lstOrders)
    {
        if(oi==oit)
        {
            oi.setSalesPrice(price);
        }
    }
}
public void removeOrderItem(OrderItem oi)
{
    lstOrders.remove(oi);
}
public double getActualOrderTotal()
{
    double sum=0.0;
   for (OrderItem oi:lstOrders)
   {
       sum=sum+oi.computeSalesPerQt(oi.getSalesPrice(),oi.getOrderQuantity());
   }
   return sum;
}
public double getTargetOrderTotal()
{
  double sum=0.0;
   for (OrderItem oi:lstOrders)
   {
       sum=sum+oi.computeSalesPerQt(oi.getTargetPrice(),oi.getOrderQuantity());
   }
   return sum;  
}


}
