/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Chaitanya
 */
public class Customer {
    private MarketRole role;
    private String affiliation;

    public MarketRole getRole() {
        return role;
    }

    public void setRole(MarketRole role) {
        this.role = role;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }
    
}
