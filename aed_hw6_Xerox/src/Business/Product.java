/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;

/**
 *
 * @author Madhuja
 */
public class Product {
    private int quantity;
    private double ceilPrice;
    private double floorPrice;
    private static int count;
    private int productId;
    private String productName;
    private String description;
   // private ArrayList<MarketOffer> marketOffers;

    
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getFloorPrice() {
        return floorPrice;
    }

    public void setFloorPrice(double floorPrice) {
        this.floorPrice = floorPrice;
    }
    
    
    
   // private HashMap<MarketRole,MarketOffer> hmOffer;

//    public HashMap<MarketRole, MarketOffer> getHmOffer() {
//        return hmOffer;
//    }
//
//    public void setHmOffer(HashMap<MarketRole, MarketOffer> hmOffer) {
//        this.hmOffer = hmOffer;
//    }
    //private Dictionary<MarketRole,MarketOffer> diMarketOffers;
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getCeilPrice() {
        return ceilPrice;
    }

    public Product() {
        count++;
        productId=count;
        //marketOffers = new ArrayList<MarketOffer>();
       // diMarketOffers=new Dictionary<MarketRole, MarketOffer>();
      // hmOffer=new HashMap<MarketRole, MarketOffer>();
    }

//    public ArrayList<MarketOffer> getMarketOffers() {
//        return marketOffers;
//    }
//
//    public void setMarketOffers(ArrayList<MarketOffer> marketOffers) {
//        this.marketOffers = marketOffers;
//    }
    
    public void setCeilPrice(double ceilPrice) {
        this.ceilPrice = ceilPrice;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
    
    @Override
    public String toString(){
        return String.valueOf(this.productId);
    }
    
}
