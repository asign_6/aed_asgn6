/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Chaitanya
 */
public class MarketList {
    private ArrayList<Market> lstMarket;

    public MarketList() {
        lstMarket=new ArrayList<Market>();
        
    }

    public ArrayList<Market> getLstMarket() {
        return lstMarket;
    }

    public void setLstMarket(ArrayList<Market> lstMarket) {
        this.lstMarket = lstMarket;
    }

    public Customer isValidCustomer(String custName)
    {
        for(Market m:lstMarket)
        {
         for(Customer c:m.getLstCustomer())
         {
             if(c.getAffiliation().equalsIgnoreCase(custName))
                 return c;
         }
        }
        return null;
    }

    
    public void addMarket()
    {
       for(MarketRole mk : MarketRole.values())
       {
           Market m=new Market();
           m.setMarketRole(mk);
           lstMarket.add(m);
       }
    
    }
    
}
